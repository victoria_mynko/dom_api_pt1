const url = "https://rickandmortyapi.com/api/character";
fetch(url)
  .then((res) => res.json())
  .then((data) => {
    const rawData = data.results;
    createTen(rawData);

    window.onscroll = function () {addScroll(rawData);};

    // удаление карточки
    document.getElementById("container").addEventListener("click", function () {
        let target = event.target;
        let length = rawData.length;

        for (i = 0; i < length; i++) {          
          if (target.name == i + 1) {
            let id = target.name;
            let div = document.getElementById(`${i+1}`);
            div.remove();
            console.log(rawData.length);            
            rawData.forEach((element, index) => {              
              if (element.id == id) {                
                rawData.splice(index, 1);                
              }
            });
          }
        }; 
      });

      // сортировка по возрастанию
      document.getElementById("date-asc").addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        AscSortDate(rawData);
        createAll(rawData);
      });

      // сортировка по убыванию
      document.getElementById("date-desc").addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        DescSortDate(rawData);
        createAll(rawData);
      });

      // сортировка по кол-ву эпизодов
      document.getElementById("episode-sort-id").addEventListener("click", function () {        
        let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        SortEpisodes(rawData);
        createAll(rawData);
      });

  }).catch((error) => {console.log(JSON.stringify(error));
  });


function createEl(character) {
  let container = document.getElementById("container");
  let wrapper = document.createElement("div");
  let charName = document.createElement("p");
  let charSpecies = document.createElement("p");
  let charCreatedPar = document.createElement("p");
  let charCreated = document.createElement("p");
  let charLocationPar = document.createElement("p");
  let charLocation = document.createElement("p");
  let charEpisodesPar = document.createElement("p");
  let charEpisodes = document.createElement("p");
  let charImg = document.createElement("img");
  let deleteBtn = document.createElement("button");


  wrapper.className = "char-wrp";
  charName.className = "par charname";
  charSpecies.className = "par";
  charCreatedPar.className = "par";
  charCreated.className = "par-text";
  charLocationPar.className = "par";
  charLocation.className = "par-text";
  charEpisodesPar.className = "par";
  charEpisodesPar.className = "par";
  charEpisodes.className = "par-text";
  charImg.className = "image";
  deleteBtn.className = "delete-btn";


  wrapper.id = character.id.toString();  
  charName.innerText = character.name;
  charSpecies.innerText = character.species;
  charCreatedPar.innerText = `Created`;
  charCreated.innerText = character.created.slice(11, 19);
  charLocationPar.innerText = `Location`;
  charLocation.innerText = `${character.location.name}`;
  charEpisodesPar.innerText = `Episodes list`;
  charEpisodes.innerText = `${numbOfEpisode(character.episode)}`;
  charImg.src = character.image;
  deleteBtn.innerText = "Delete";  
  deleteBtn.setAttribute("name", character.id.toString());



  wrapper.appendChild(deleteBtn);
  wrapper.appendChild(charImg);
  wrapper.appendChild(charName);
  wrapper.appendChild(charSpecies);
  wrapper.appendChild(charCreatedPar);
  wrapper.appendChild(charCreated);
  wrapper.appendChild(charLocationPar);
  wrapper.appendChild(charLocation);
  wrapper.appendChild(charEpisodesPar);
  wrapper.appendChild(charEpisodes);
  
  container.appendChild(wrapper);
}

const openingElementsNumber = 10;

function addScroll(Object) {
  if (document.getElementsByClassName("char-wrp").length < 19) {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {      
      let length = document.getElementsByClassName("char-wrp").length;
        for (i = 0; i < length; i++) {          
          let div = document.querySelector(".char-wrp");
          div.remove();
        }
        return Object.map((character) => {
          createEl(character);
        });  
    }
  }
}

function AscSortDate(Object) {
  Object.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return -1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return 1;
    }
    return 0;
  });
}

function DescSortDate(Object) {
  Object.sort(function (a, b) {
    if (a.created.slice(11, 19) > b.created.slice(11, 19)) {
      return 1;
    }
    if (a.created.slice(11, 19) < b.created.slice(11, 19)) {
      return -1;
    }
    return 0;
  });
}

function SortEpisodes(Object) {
  Object.sort(function (a, b) {
    if (a.episode.length > b.episode.length) {
      return -1;
    }
    if (a.episode.length < b.episode.length) {
      return 1;
    }
    return 0;
  }); 
}

function numbOfEpisode(episode) {
  let episodesList = [];
  episode.map((string) => {
    let stringParts = string.split("/");
    episodesList.push(stringParts[stringParts.length - 1]);
  });
  return episodesList.join(", ");
}

function createTen(Object) {
  return Object.slice(0, openingElementsNumber).map((character) => {
    createEl(character);
  });
}

function createAll(Object) {
  return Object.map((character) => {
    createEl(character);
  });
}